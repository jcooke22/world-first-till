# World First till system #
========================

## Prerequisites ##
* PHP 5.5.24 or greater (The PHP timezone (date.timezone) must be set for Symfony console to function correctly)
* Composer (https://getcomposer.org/)
* git
* PHP Unit (4.7.0 is compatible with PHP 5.5.24)

## Installation ##
### Clone the repository ###

```
#!bash
git clone https://jcooke22@bitbucket.org/jcooke22/world-first-till.git
cd world-first-till
```

### Install the dependencies ###

```
#!bash
composer install
#The Symfony install script will ask for some parameter values "Some parameters are missing. Please provide them.". These are not important for this project, just hit enter on each to use the default values.
```

## Running the till system ##
### Process GBP transaction ###

```
#!bash
php app/console till:process-transaction tesco.json
```

### Process USD transaction ###

```
#!bash
php app/console till:process-transaction walmart.json
```

### Running unit tests ###

```
#!bash
phpunit -c app/
```