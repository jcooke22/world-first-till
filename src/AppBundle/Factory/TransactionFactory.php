<?php

namespace AppBundle\Factory;

use AppBundle\Inter;
use AppBundle\Model;

/**
 * Class TransactionFactory
 * @package AppBundle\Factory
 */
class TransactionFactory
{
    /**
     * Static factory method to create an instance of Model\Transaction 
     *
     * @author James Cooke
     * @param Inter\LoaderInterface $loader
     * @return Model\Transaction
     */
    public static function getInstance(Inter\LoaderInterface $loader) {
        // Get the raw object
        $rawTransactionObject = $loader->getObject();
        
        // Instantiate a new transaction
        $transaction = new Model\Transaction();
        
        // Get the meta data
        $transaction->transactionId        = $rawTransactionObject->transactionId; 
        $transaction->storeName            = $rawTransactionObject->storeName;
        $transaction->currency             = $rawTransactionObject->currency;
        
        // Get the items
        foreach ($rawTransactionObject->transactionItems as $rawItem) {
            $transactionItem = new Model\TransactionItem();
            $transactionItem->name = $rawItem->name;
            $transactionItem->value = $rawItem->value;;
            $transaction->addItem($transactionItem); 
        }
        
        // Get the discounts
        foreach ($rawTransactionObject->transactionDiscounts as $rawDiscount) {
            $transactionDiscount = new Model\TransactionDiscount();
            $transactionDiscount->name = $rawDiscount->name;
            $transactionDiscount->value = $rawDiscount->value;
            $transaction->addDiscount($transactionDiscount);
        }
        
        // Return the transaction
        return $transaction;
    }
}