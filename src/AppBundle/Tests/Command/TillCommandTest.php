<?php
use AppBundle\Command;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class TillCommandTest
 */
class TillCommandTest extends KernelTestCase
{
    /**
     * Test for the Tesco transaction
     *
     * @author James Cooke
     */
    public function testTesco()
    {
        $kernel = $this->createKernel();
        $kernel->boot();
        $application = new Application($kernel);
        $application->add(new Command\TillCommand());
        
        $command = $application->find('till:process-transaction');
        
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'command' => $command->getName(),
            'transaction-filename' => 'tesco.json'
        ));

        $this->assertRegExp('/\£3.77/', $commandTester->getDisplay());
    }

    /**
     * Test for the Walmart transaction
     *
     * @author James Cooke
     */
    public function testWalmart()
    {
        $kernel = $this->createKernel();
        $kernel->boot();
        $application = new Application($kernel);
        $application->add(new Command\TillCommand());
        
        $command = $application->find('till:process-transaction');
        
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'command' => $command->getName(),
            'transaction-filename' => 'walmart.json'
        ));

        $this->assertRegExp('/\$22.60/', $commandTester->getDisplay());
    }
}