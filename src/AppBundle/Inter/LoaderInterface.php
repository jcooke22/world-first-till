<?php
namespace AppBundle\Inter;

/**
 * Interface LoaderInterface
 * @package AppBundle\Inter
 */
interface LoaderInterface
{
    public function load($filepath);
    public function getObject();
}