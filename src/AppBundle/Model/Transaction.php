<?php

namespace AppBundle\Model;

use AppBundle\Inter;
use AppBundle\Model;

/**
 * Class Transaction
 * @package AppBundle\Model
 */
class Transaction
{
    public $transactionId           = null;
    public $storeName               = null;
    public $currency                = null;
    protected $transactionItems     = array();
    protected $transactionDiscounts = array();
    public $subTotal                = null;
    public $discountTotal           = null;
    public $grandTotal              = null;


    /**
     * Add a transaction item to the collection
     *
     * @author James Cooke
     * @param TransactionItem $item
     */
    public function addItem(Model\TransactionItem $item)
    {
        $this->transactionItems[] = $item;
        $item->applyValueToTransaction($this);
    }

    /**
     * Add a transaction discount to the collection
     *
     * @author James Cooke
     * @param TransactionDiscount $discount
     */
    public function addDiscount(Model\TransactionDiscount $discount)
    {
        $this->transactionDiscounts[] = $discount;
        $discount->applyValueToTransaction($this);
    }

    /**
     * Generates and returns an array for output based on $this transaction
     * 
     * @author James Cooke
     * @todo - Move this into a output class
     */
    public function getOutputTableArray()
    {
        // Define the array
        $tableArray = array();

        // Store name
        $tableArray[] = array('Store name', $this->storeName);

        // Transaction ID
        $tableArray[] = array('Transaction ID', $this->transactionId);

        // Padding
        $tableArray[] = array('----------', '----------');
        
        // Items
        foreach ($this->transactionItems as $item) {
            $tableArray[] = array($item->name, $this->_formatValue($item->value));
        }
        
        // Padding
        $tableArray[] = array('----------', '----------');

        // Sub-Total
        $tableArray[] = array('Sub-Total', $this->_formatValue($this->subTotal));

        // Discounts
        $tableArray[] = array('Discounts', $this->_formatValue($this->discountTotal));
        
        // Padding
        $tableArray[] = array('----------', '----------');
        
        // Grand total
        $tableArray[] = array('Grand Total', $this->_formatValue($this->grandTotal));
        
        // Return the array
        return $tableArray;
    }

    /**
     * Prepare a value for output
     *
     * @author James Cooke
     * @param $value
     * @return string
     */
    private function _formatValue($value)
    {
        return money_format('%n', $value / 100);
    }
}