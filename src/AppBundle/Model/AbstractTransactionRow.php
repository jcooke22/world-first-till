<?php

namespace AppBundle\Model;

use AppBundle\Model;

/**
 * Class AbstractTransactionRow
 * @package AppBundle\Model
 */
abstract class AbstractTransactionRow
{
    public $name  = null;
    public $value = 0;

    /**
     * Bind the value of this abstract transaction row to the transaction
     *
     * @author James Cooke
     * @param Transaction $transaction
     */
    public function applyValueToTransaction(Model\Transaction $transaction)
    {
        // Apply $this->value to the $transaction
    }
}