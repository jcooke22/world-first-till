<?php

namespace AppBundle\Model;

use AppBundle\Model;

/**
 * Class TransactionItem
 * @package AppBundle\Model
 */
class TransactionItem extends Model\AbstractTransactionRow
{
    /**
     * Increase the sub total of the transaction.
     * Increase the grand total of the transaction. 
     *
     * @author @james.cooke@acknowledgement.co.uk James Cooke
     * @param Transaction $transaction
     */
    public function applyValueToTransaction(Model\Transaction $transaction)
    {
        $transaction->subTotal   = $transaction->subTotal + $this->value; 
        $transaction->grandTotal = $transaction->grandTotal + $this->value;
    }
}