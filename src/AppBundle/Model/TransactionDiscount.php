<?php

namespace AppBundle\Model;

use AppBundle\Model;

/**
 * Class TransactionDiscount
 * @package AppBundle\Model
 */
class TransactionDiscount extends Model\AbstractTransactionRow
{
    /**
     * Reduce the grand total of the transaction.
     * Increase the discount total of the transaction.
     *
     * @author James Cooke
     * @param Transaction $transaction
     */
    public function applyValueToTransaction(Model\Transaction $transaction)
    {
        $transaction->grandTotal = $transaction->grandTotal - $this->value; 
        $transaction->discountTotal = $transaction->discountTotal + $this->value; 
    }
}