<?php

namespace AppBundle\Loader;

use AppBundle\Inter;

/**
 * Class LoaderJson
 * @package AppBundle\Loader
 */
class LoaderJson implements Inter\LoaderInterface
{
    /**
     * The raw data for a transaction
     * @var null | StdClass
     */
    protected $object = null;

    /**
     * Load data from a JSON file into $this->object.
     * Sets the currency local for output.
     * @author James Cooke
     * @param $filename
     */
    public function load($filename)
    {
        $this->object = json_decode(file_get_contents($filename));
        switch ($this->object->currency) {
            case  'GBP':
                setlocale(LC_MONETARY, 'en_GB');
                break;
            case  'USD':
                setlocale(LC_MONETARY, 'en_US');
                break;
        }
    }

    /**
     * Public getter for the raw object
     * @author James Cooke
     * @return StdClass|null
     */
    public function getObject()
    {
        return $this->object;
    }
}