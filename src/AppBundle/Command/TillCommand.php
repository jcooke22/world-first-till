<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Config\FileLocator;

use AppBundle\Model;
use AppBundle\Loader;
use AppBundle\Factory;

/**
 * Class TillCommand
 * @package AppBundle\Command
 */
class TillCommand extends ContainerAwareCommand
{
    private $container;

    /**
     * Set up the command
     * @author James Cooke
     */
    protected function configure()
    {
        $this
            ->setName('till:process-transaction')
            ->setDescription('Process a TILL transaction')
            ->addArgument(
                'transaction-filename',
                InputArgument::REQUIRED,
                'What is the filename of the transaction to process? Available files are in app/assets/transactions'
            )
        ;
    }

    /**
     * Run the command
     * 
     * @author James Cooke
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Get the container
        $this->container = $this->getApplication()->getKernel()->getContainer();
        
        // Get the filename
        $transactionFilename = $input->getArgument('transaction-filename');
        
        // Get the full filepath
        $transactionDir = array($this->container->get('kernel')->getRootDir() . '/assets/transactions');
        $locator = new FileLocator($transactionDir);
        $filepath = $locator->locate($transactionFilename);
        
        // Get a loader
        $loader = new Loader\LoaderJson();
        $loader->load($filepath);
        
        // Get an instance of the transaction
        $transaction = Factory\TransactionFactory::getInstance($loader);

        // Get the output table
        // @todo - Move this into a output class
        $table = $this->getHelper('table');
        $table
            ->setHeaders(array('Item', 'Price'))
            ->setRows($transaction->getOutputTableArray())
        ;
        // Render the table
        $table->render($output);
    }
}